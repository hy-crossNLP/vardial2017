#-*-makefile-*-
#
#---------------------------------------------------------
# Makefile for creating data sets and baseline models
# for the shared task in cross-lingual dependency parsing
# at VarDial 2017
#---------------------------------------------------------
# Author: Jörg Tiedemann <jorg.tiedemann@helsinki.fi>
# Date: December 18, 2016
# Last Modified: January 5, 2017
#---------------------------------------------------------
# USAGE:
#
#   make all .......... make all targets for dev and test
#   make traindata .... make training data
#   make devdata ...... make development data
#   make testdata ..... make test data
#   make base-models .. make all basic tagger and parser models
#
# targets that depend on settings of SRC and TRG:
#
#   make crossdev ..... make crosslingual baselins on dev
#   make crosstest .... make crosslingual baselins on test
#
# Set SRC and TRG to run with different languages, e.g.
#   make SRC=da TRG=no crossdev
#
#   make all-crossdev . make crossdev for all languages
#   make all-crosstest  make crosstest for all languages
#
#
# DEPENDENCIES:
#
#   - UD-treebanks v1.4 in subdir <ud-treebanks-v1.4>
#     available from https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-1827/ud-treebanks-v1.4.tgz
#   - udpipe installed in PATH
#     available from https://github.com/ufal/udpipe/releases/download/v1.0.0/udpipe-1.0.0-bin.zip
#
#---------------------------------------------------------



SRC = sl
TRG = hr

UD_TREEBANKS = ud-treebanks-v1.4

DEVDATA = 	treebanks/sk-ud-predPoS-dev.conllu \
		treebanks/no-ud-predPoS-dev.conllu \
		treebanks/hr-ud-predPoS-dev.conllu

TESTDATA = 	treebanks/sk-ud-predPoS-test.conllu \
		treebanks/no-ud-predPoS-test.conllu \
		treebanks/hr-ud-predPoS-test.conllu

TRAINDATA = 	treebanks/cs-ud-train.conllu \
		treebanks/da-ud-train.conllu \
		treebanks/sv-ud-train.conllu \
		treebanks/sl-ud-train.conllu \
		treebanks/dasv-ud-train.conllu

TAGGER_MODELS = models/hr.tagger.udpipe \
		models/no.tagger.udpipe \
		models/sk.tagger.udpipe \
		models/da.tagger.udpipe \
		models/sl.tagger.udpipe \
		models/sv.tagger.udpipe

#		models/cs.tagger.udpipe \

PARSER_MODELS = models/hr.parser.udpipe \
		models/no.parser.udpipe \
		models/sk.parser.udpipe \
		models/cs.parser.udpipe \
		models/da.parser.udpipe \
		models/sl.parser.udpipe \
		models/sv.parser.udpipe


DELEX_MODELS = ${patsubst %.parser.udpipe,%-delex.parser.udpipe,${PARSER_MODELS}}


.PHONY: all traindata devdata testdata
.PHONY: crossdev crosstest all-crossdev all-crosstest
.PHONY: delexdev delextest uposdev upostest

all: all-crossdev all-crosstest

traindata: ${TRAINDATA}
devdata: ${DEVDATA}
testdata: ${TESTDATA}

all-crossdev:
	make SRC=sl TRG=hr crossdev delexdev
	make SRC=cs TRG=sk crossdev delexdev
	make SRC=da TRG=no crossdev delexdev
	make SRC=sv TRG=no crossdev delexdev
	make SRC=dasv TRG=no crossdev delexdev
	make SRC=hr TRG=hr crossdev
	make SRC=sk TRG=sk crossdev
	make SRC=no TRG=no crossdev

all-crosstest:
	make SRC=sl TRG=hr crosstest delextest
	make SRC=cs TRG=sk crosstest delextest
	make SRC=da TRG=no crosstest delextest
	make SRC=sv TRG=no crosstest delextest
	make SRC=dasv TRG=no crosstest delextest
	make SRC=hr TRG=hr crosstest
	make SRC=sk TRG=sk crosstest
	make SRC=no TRG=no crosstest

all-uposdev:
	make SRC=sl TRG=hr uposdev
	make SRC=da TRG=no uposdev
	make SRC=sv TRG=no uposdev
	make SRC=dasv TRG=no uposdev
	make SRC=cs TRG=sk uposdev

all-upostest:
	make SRC=sl TRG=hr upostest
	make SRC=da TRG=no upostest
	make SRC=sv TRG=no upostest
	make SRC=dasv TRG=no upostest
	make SRC=cs TRG=sk upostest

crossdev: results/${TRG}.${SRC}.dev.eval
crosstest: results/${TRG}.${SRC}.test.eval

delexdev: results/${TRG}.${SRC}-delex.dev.eval
delextest: results/${TRG}.${SRC}-delex.test.eval

# models with universal PoS tags only (no morphology)
uposdev: results/${TRG}.${SRC}-upos.dev.eval
upostest: results/${TRG}.${SRC}-upos.test.eval


treebanks: ${TRAINDATA} ${DEVDATA} ${TESTDATA}
base-models: ${TAGGER_MODELS} ${PARSER_MODELS}


# make sure that tagger and parser models and devtest data
# are not immediately deleted when used as intermediate files
.SECONDARY: ${TAGGER_MODELS} ${PARSER_MODELS} ${DELEX_MODELS}
.SECONDARY: ${TRAINDATA} ${DEVDATA} ${TESTDATA}


# tagger models for predicting PoS and morphology in dev and test data

models/sk.tagger.udpipe:
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--parser=none \
		${UD_TREEBANKS}/UD_Slovak/sk-ud-train.conllu

models/hr.tagger.udpipe:
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--parser=none \
		${UD_TREEBANKS}/UD_Croatian/hr-ud-train.conllu

models/no.tagger.udpipe:
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--parser=none \
		${UD_TREEBANKS}/UD_Norwegian/no-ud-train.conllu

models/%.tagger.udpipe: treebanks/%-ud-train.conllu
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--parser=none \
		$<


# parser models
# (1) target languages on UD data as upper bound models
# (2) source languages for baseline transfer models

models/sk.parser.udpipe:
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--tagger=none \
		${UD_TREEBANKS}/UD_Slovak/sk-ud-train.conllu

models/hr.parser.udpipe:
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--tagger=none \
		${UD_TREEBANKS}/UD_Croatian/hr-ud-train.conllu

models/no.parser.udpipe:
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--tagger=none \
		${UD_TREEBANKS}/UD_Norwegian/no-ud-train.conllu

models/%.parser.udpipe: treebanks/%-ud-train.conllu
	mkdir -p $(dir $@)
	udpipe 	--train $@ \
		--tokenizer=none \
		--tagger=none \
		$<




# data sets with predicted PoS tags

treebanks/sk-ud-predPoS-%.conllu: models/sk.tagger.udpipe \
			${UD_TREEBANKS}/UD_Slovak/sk-ud-%.conllu
	mkdir -p $(dir $@)
	udpipe -input=conllu -tag $^ > $@
	cp ${UD_TREEBANKS}/UD_Slovak/LICENSE.txt ${dir $@}LICENSE-SK.txt

treebanks/hr-ud-predPoS-%.conllu: models/hr.tagger.udpipe \
			${UD_TREEBANKS}/UD_Croatian/hr-ud-%.conllu
	mkdir -p $(dir $@)
	udpipe -input=conllu -tag $^ > $@
	cp ${UD_TREEBANKS}/UD_Croatian/LICENSE.txt ${dir $@}LICENSE-HR.txt

treebanks/no-ud-predPoS-%.conllu: models/no.tagger.udpipe \
			${UD_TREEBANKS}/UD_Norwegian/no-ud-%.conllu
	mkdir -p $(dir $@)
	udpipe -input=conllu -tag $^ > $@
	cp ${UD_TREEBANKS}/UD_Norwegian/LICENSE.txt ${dir $@}LICENSE-NO.txt



# copies of the training data

treebanks/cs-ud-train.conllu: ${UD_TREEBANKS}/UD_Czech/cs-ud-train.conllu
	mkdir -p $(dir $@)
	cp $< $@
	cp ${dir $<}LICENSE.txt ${dir $@}LICENSE-CS.txt

treebanks/da-ud-train.conllu: ${UD_TREEBANKS}/UD_Danish/da-ud-train.conllu
	mkdir -p $(dir $@)
	cp $< $@
	cp ${dir $<}LICENSE.txt ${dir $@}LICENSE-DA.txt

treebanks/sv-ud-train.conllu: ${UD_TREEBANKS}/UD_Swedish/sv-ud-train.conllu
	mkdir -p $(dir $@)
	cp $< $@
	cp ${dir $<}LICENSE.txt ${dir $@}LICENSE-SV.txt

treebanks/sl-ud-train.conllu: ${UD_TREEBANKS}/UD_Slovenian/sl-ud-train.conllu
	mkdir -p $(dir $@)
	cp $< $@
	cp ${dir $<}LICENSE.txt ${dir $@}LICENSE-SL.txt

treebanks/dasv-ud-train.conllu: treebanks/da-ud-train.conllu treebanks/sv-ud-train.conllu
	mkdir -p $(dir $@)
	cat $^ > $@

# delexicalized models

treebanks/%-delex-ud-train.conllu: treebanks/%-ud-train.conllu
	tools/delexicalize.pl < $< > $@

treebanks/%-upos-ud-train.conllu: treebanks/%-ud-train.conllu
	tools/delexicalize.pl -u < $< > $@


# baseline results for cross-lingual parsing

results/${TRG}.${SRC}.%.eval: models/${SRC}.parser.udpipe \
			treebanks/${TRG}-ud-predPoS-%.conllu
	mkdir -p $(dir $@)
	udpipe -input=conllu -parse $^ > $(@:.eval=.conllu)
	tools/eval.pl $(word 2,$^) $(@:.eval=.conllu) > $@

results/${TRG}.${SRC}-delex.%.eval: models/${SRC}-delex.parser.udpipe \
			treebanks/${TRG}-ud-predPoS-%.conllu
	mkdir -p $(dir $@)
	tools/delexicalize.pl < $(word 2,$^) > $@.in
	udpipe -input=conllu -parse $< $@.in > $(@:.eval=.conllu)
	rm -f $@.in
	tools/eval.pl $(word 2,$^) $(@:.eval=.conllu) > $@

results/${TRG}.${SRC}-upos.%.eval: models/${SRC}-upos.parser.udpipe \
			treebanks/${TRG}-ud-predPoS-%.conllu
	mkdir -p $(dir $@)
	tools/delexicalize.pl -u < $(word 2,$^) > $@.in
	udpipe -input=conllu -parse $< $@.in > $(@:.eval=.conllu)
	rm -f $@.in
	tools/eval.pl $(word 2,$^) $(@:.eval=.conllu) > $@


