
# Baseline results for cross-lingual parsing at VarDial 2017

The files *.eval include the evaluation scores for baseline models in `../models` applied to development and test data with predicted PoS labels and predicted morphological features in `../treebanks`. The following models have been tested:

* xx.xx.dev.conllu: fully supervised model for language <xx> tested on development data
* yy.xx.dev.conllu: model for language <xx> tested on development data of language <yy>
* yy.xx-delex.dev.conllu: delexicalized model for language <xx> tested on development data of language <yy>
* yy.xx-upos.dev.conllu: delexicalized model for language <xx> with universal PoS only tested on development data of language <yy>

