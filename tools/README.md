
# Tools for the shared task on cross-lingual dependency parsing at VarDial 2017


* `eval07.pl`:               CoNLL-07 evaluation script
* `eval.pl`:                 VarDial 2017 evaluation wrapper script
* `delexicalize.pl`:         delexicalize conllu files
* `conllu-to-conllx.pl`:     convert conllu to conllx format (taken from ud-tools-v1.4)


