#!/usr/bin/perl
#
# make sure that columns are not empty strings or spaces only
#

while (<>){
    unless (/^\s*$/){
	my @a=split(/\t/);
	foreach (0..$#a){
	    $a[$_] = '_' unless $a[$_];
	}
	$_ = join("\t",@a);
    }
    print;
}
