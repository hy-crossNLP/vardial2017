#!/usr/bin/perl
#
# -X .... skip XML escapes for &<>"'
#

use Getopt::Std;

our($opt_X);
getopts('X');


my @ids=();
my %words=();
my %rels=();

my %yields=();

my $count=0;

# print "<corpus>\n";

while (<>){
    chomp;
    if (/^\s*$/){
	&print_sentence();
	@ids=();%rels=();%words=();%yields=();
	$count++;
	if (! ($count % 500)){print STDERR "."}
	if (! ($count % 10000)){print STDERR "$count\n"}
	next;
    }
    my ($id,$word,$lemma,$ctag,$tag,$feat,
	$head,$deprel,$phead,$pdeprel) = split (/\t+/);

    push(@ids,$id);
    $words{$id}{word}=&xmlify($word);
    $words{$id}{tag}=&xmlify($tag);
    $words{$id}{deprel}=&xmlify($deprel);

    $rels{$head}{$id}=$deprel;
}

# print "</corpus>\n";

sub xmlify {
    unless ($opt_X){
        $_[0] =~ s/\&/&amp;/gs;
        $_[0] =~ s/\</&lt;/gs;
        $_[0] =~ s/\>/&gt;/gs;
        $_[0] =~ s/\"/&quot;/gs;
        $_[0] =~ s/\'/&apos;/gs;
    }
    return $_[0];
}






sub print_sentence{

    # get all spans by looking at the yield for each token

    my %forward=();
    my %backward=();

    foreach my $id (@ids){
	my @yield = &yield($id);
	$forward{$yield[0]}{$yield[-1]}=$words{$id}{deprel};
	$backward{$yield[-1]}{$yield[0]}=$words{$id}{deprel};
    }

    &print_tree(\%forward,\%backward);
}


sub print_tree{
    my ($forward,$backward) = @_;

    foreach my $id (@ids){

	# constituents start here
	if (exists $$forward{$id}){
	    foreach my $end (sort { $b <=> $a } keys %{$$forward{$id}}){
		print " <tree label=\"$$forward{$id}{$end}\">"
	    }
	}

	print " <tree label=\"$words{$id}{tag}\"> $words{$id}{word} </tree>";

	# constituents end here ...
	if (exists $$backward{$id}){
	    foreach my $start (sort { $b <=> $a } keys %{$$backward{$id}}){
		print " </tree>"
	    }
	}
    }
    print "\n";
}



sub yield{
    my $id = shift;

    # I know the yield or this node already? --> just return it
    if (exists $yields{$id}){
	return @{$yields{$id}};
    }

    my @nodes = ($id);
    if (exists $rels{$id}){
	foreach my $n (keys %{$rels{$id}}){
	    my @children = &yield($n);
	    push(@nodes,@children);
	}
    }
    @{$yields{$id}} = sort {$a <=> $b } @nodes;
    return @{$yields{$id}};
}



