#!/usr/bin/perl
#
# annotation projection of dependency relations with word alignment backoff
#----------------------------------------------------------------------------
# USAGE
#
# project-deprels.pl [OPTIONS] \
#            source-treebankfile target-corpus \
#            alignment.intersection alignment.gdfa
#
# source-treebankfile      annotated source language in CoNLL format
# target-corpus            translations into target language (1 sentence/line)
# alignment.intersection   high precision word alignment (Moses format)
# alignment.gdfa           high recall word alignment (Moses format)
#
#
# OPTIONS
#
# -M .......... one-to-many without MWE's
# -A ratio .... alignment ratio threshold
# -f .......... project other features as well (morph, lemma, ...)
#
#----------------------------------------------------------------------------


use strict;
use Getopt::Std;
use File::Basename qw/basename/;

our($opt_A,$opt_a,$opt_e,$opt_d,$opt_D,$opt_f,$opt_u,$opt_t,
    $opt_M,$opt_n,$opt_N,$opt_s,$opt_x,$opt_T,$opt_r);
getopts('A:adefDutTMnNsx:r');


my $treebank = shift(@ARGV);
my $corpus   = shift(@ARGV);
my ($AlgInter,$AlgGDFA);
if ($#ARGV){
    $AlgInter = shift(@ARGV);
    $AlgGDFA  = shift(@ARGV);
}
elsif (@ARGV){
    $AlgGDFA  = shift(@ARGV);
}
else{
    die "no alignment or n-best translations given!\n";
}

# my $AlgS2T   = shift(@ARGV);
# my $AlgT2S   = shift(@ARGV);


my $reverse = $opt_r || 0;

# need to reverse word alignments!
# if ($AlgInter){
#     if ($AlgInter=~/^(..)\.(..)-(..)/){
# 	$reverse = 1 if ($1 eq $2);
#     }
# }
my $basename = basename($AlgGDFA);
if ($basename=~/^(..)\.(..)-(..)/){
    $reverse = 1 if ($1 eq $2);
    print STDERR "reverse alignment!!!!\n";
}


open F,"<$treebank" || die "cannot open treebank!\n";

if ($corpus=~/\.gz$/){ 
    open C,"gzip -cd < $corpus |" || die "cannot open $corpus"; 
}
else{ 
    open C,"<$corpus" || die "cannot open $corpus"; 
}
if ($AlgInter){
    if ($AlgInter=~/\.gz$/){ 
        open AI,"gzip -cd < $AlgInter |" || die "cannot open $AlgInter"; 
    }
    else{ 
        open AI,"<$AlgInter" || die "cannot open $AlgInter"; 
    }
}
if ($AlgGDFA=~/\.gz$/){ 
    open AG,"gzip -cd < $AlgGDFA |" || die "cannot open $AlgGDFA"; 
}
else{ 
    open AG,"<$AlgGDFA" || die "cannot open $AlgGDFA"; 
}


my %countTrans=();
my $sentNr=0;


##################################################################

my $EOF=0;

while (!$EOF){

    #-------------------------------------------------------------
    # read the next sentence from the source language treebank
    #  @sent = array of source sentence attributes (CONLL columns)
    #  %srcTree = source sentence tree 
    #             ( key = word_id, value = head_word_id )
    #-------------------------------------------------------------

    my @sent=();
    $EOF=1;
    while (<F>){
        last if (/^\s$/);
        push(@sent,[split(/\t/,$_)]);
        $EOF=0;
    }

    my %srcTree=();
    foreach my $w (0..$#sent){
        $srcTree{$w+1} = $sent[$w][6];
    }

    my $trgSent = <C>;
    ## !!!!!!!!!!!!!!!!!! NEW 2014-11-10 !!!!!!!!!!
    ## upper-case first letter!
    $trgSent = ucfirst($trgSent);
    my @words = split(/\s+/,$trgSent);

    $sentNr++;
    my %trgTree=();


    #-----------------------
    # read word alignments

    my %interSrc2Trg=();
    my %interTrg2Src=();
    my $nrInterAlg = 0;
    if ($AlgInter){
        my $algI=<AI>;
        $algI = reverse_alignment($algI) if ($reverse);
        $nrInterAlg = read_alignment($algI,\%interSrc2Trg,\%interTrg2Src);
    }

    my %wordSrc2Trg=();
    my %wordTrg2Src=();
    my $nrWordAlg = 0;

    my $algG=<AG>;
    $algG = reverse_alignment($algG) if ($reverse);
    $nrWordAlg = read_alignment($algG,\%wordSrc2Trg,\%wordTrg2Src);

    #----------------------------------------------------------------------------

    %trgTree=();

    if ($opt_s){
        $countTrans{OneToMany}    += one_to_many(\@sent,\@words,
                                                 \%wordSrc2Trg,\%wordTrg2Src,
                                                 \%srcTree,\%trgTree);
    }
    else{
        $countTrans{OneToMany}    += one_to_many_mwe(\@sent,\@words,
                                                     \%interSrc2Trg,\%interTrg2Src,
                                                     \%wordSrc2Trg,\%wordTrg2Src,
                                                     \%srcTree,\%trgTree);
    }
    $countTrans{ManyToOne}    += many_to_one(\@sent,\@words,
                                             \%wordSrc2Trg,\%wordTrg2Src,
                                             \%srcTree,\%trgTree);
    $countTrans{UnalignedSrc} += unaligned(\@sent,\@words,
                                           \%wordSrc2Trg,\%wordTrg2Src,
                                           \%srcTree,\%trgTree);
    $countTrans{OneToOne}     += one_to_one(\@sent,\@words,
                                            \%wordSrc2Trg,\%wordTrg2Src,
                                            \%srcTree,\%trgTree);

    # ... leave unaligned target words out of the projected tree!
    # delete undary DUMMY nodes (one incoming & one outgoing arc)
    unless ($opt_u){
        $countTrans{DeletedUnaryDummies} += &delete_unary_dummies(\%trgTree,\@words,\@sent);
        $countTrans{DeletedDummyLeaves} += &delete_dummy_leaves(\%trgTree,\@words,\@sent);
    }

    unless (check_tree_properties(\%trgTree,1)){
        print STDERR "($sentNr) Projected structure is not a tree! --> skip it!\n";
        $countTrans{SkipNonTree}++;
        next;
    }

    my $algRatio = 0;
    if ($nrWordAlg>0){
	$algRatio = $nrInterAlg/$nrWordAlg;
    }
    if ($opt_A){
	if ($algRatio<$opt_A){
	    $countTrans{SkipBadAlgRatio}++;
	    next;
	}
    }

    if (&print_projected_tree(\@words,\@sent,\%trgTree)){
        $countTrans{successfull}++;
	print STDERR "ALIGNRATIO = ",$algRatio,"\n";
    }
    if ($opt_x && ($opt_x<=$countTrans{successfull})){
        exit(0);
    }
}


foreach (keys %countTrans){
    printf STDERR " %10d %s\n", $countTrans{$_},$_;
}



##################################################################


sub print_projected_tree{
    my ($words,$sent,$trgTree) = @_;

    # print the projected tree
    # 1) map words to positions (some target words may be deleted)
    # 2) copy features and dep information
    # 3) print in CONLL format

    my %map=();
    my $nr = 0;
    foreach my $f (sort {$$trgTree{$a}{position} <=> $$trgTree{$b}{position}} keys %{$trgTree}){
        $nr++;
        $map{$f}=$nr;
    }

    # count how many target words will be ignored
    foreach my $f (0..$#{$words}){
        unless (exists $$trgTree{$f}){
            ## ignore word ---> print a message?!
            $countTrans{UnalignedTrg}++;
        }
    }

    my $linesPrinted=0;
    my $sentence='';
    my $countDummyWords=0;
    my $countDummyTags=0;
    foreach my $f (sort {$$trgTree{$a}{position} <=> $$trgTree{$b}{position}} 
                   keys %{$trgTree}){

        next unless (defined $$trgTree{$f}{head});

        my @cols=();
        my $word = ($f == int($f)) ? $$words[$f] : 'DUMMY';
        $countDummyWords++ if ($word eq 'DUMMY');

        if (defined $$trgTree{$f}{sDep}){
            @cols = @{$$sent[$$trgTree{$f}{sDep}]};
        }
        else{
            @cols = ($f+1,$word,'_','DUMMY','DUMMY','_',0,'DUMMY','_',"_\n");
##            # NEW: label for dummy relations = mwe!
#            @cols = ($f+1,$word,'_','DUMMY','DUMMY','_',0,'mwe','_',"_\n");
            $countDummyTags++;
        }
        $cols[0] = $map{$f};
        $cols[1] = $word;
        $cols[6] = $$trgTree{$f}{head} == -1 ? 0 : $map{$$trgTree{$f}{head}};
	unless ($opt_f){
	    $cols[2] = '_';
	    $cols[4] = '_';
	    $cols[5] = '_';
	    $cols[9] = "_\n";
	}
#        print join("\t",@cols);
        $sentence .= join("\t",@cols);
        $linesPrinted++;
    }
    if ($countDummyWords){
        $countTrans{IncludesDummyWord}++;
        if ($opt_n){
            return 0;
        }
    }
    if ($countDummyTags){
        $countTrans{IncludesDummyTag}++;
        if ($opt_N){
            return 0;
        }
    }
    print $sentence;
    print "\n" if ($linesPrinted);
    return $linesPrinted;
}


sub reverse_alignment{
    my $alg=shift;
    my @links = split(/\s+/,$alg);
    foreach (0..$#links){
        $links[$_]=~s/^([0-9]+)-([0-9]+)$/$2-$1/;
    }
    $alg = join(' ',@links);
    return $alg;
}


sub read_alignment{
    my $algstring = shift;
    my $wordSrc2Trg = shift;
    my $wordTrg2Src = shift;

    # read the word alignment
    my @parts = split(/\s+/,$algstring);
    %{$wordSrc2Trg} = ();
    %{$wordTrg2Src} = ();

    foreach (@parts){
        if (/^([0-9]+)\-([0-9]+)/){
            my ($from,$to) = ($1,$2);
            # for tree-based models: fix align info to be 
            # compatible with phrase-based systems
            if ($opt_t){
                $from--;$to--;
            }
            $$wordSrc2Trg{$from}{$to}=1;
            $$wordTrg2Src{$to}{$from}=1;
        }
    }
    return scalar @parts;
}



sub get_distance_to_root{
    my ($w,$tree) = @_;
    my $steps = 0;
    my %visited = ($w => 1);
    while ($$tree{$w} != 0){
        $steps++;
        $w = $$tree{$w};
        if (exists $visited{$w}){
            print STDERR "tree includes cycle! ($w)\n";
            last;
        }
        $visited{$w}=1;
    }
    return $steps;
}


sub one_to_one{
    my ($srcWords,$trgWords,$wordSrc2Trg,$wordTrg2Src,$srcTree,$trgTree) = @_;

    # one2one
    # - simply copy the dependency relation!

    my $proj=0;
    foreach my $ei (sort {$a <=> $b} keys %{$srcTree}){
        my $ej = $$srcTree{$ei};
        if ((exists $$wordSrc2Trg{$ei-1}) && 
            (keys %{$$wordSrc2Trg{$ei-1}} == 1)){
            my ($fx) = keys %{$$wordSrc2Trg{$ei-1}};
            $proj++;
            if ($ej == 0){
                $$trgTree{$fx}{head} = -1;
                $$trgTree{$fx}{sHead} = -1;
            }
            elsif ((exists $$wordSrc2Trg{$ej-1}) && 
                   (keys %{$$wordSrc2Trg{$ej-1}} == 1)){
                my ($fy) = keys %{$$wordSrc2Trg{$ej-1}};
                $$trgTree{$fx}{head} = $fy;
            }
            $$trgTree{$fx}{sDep} = $ei-1;
            $$trgTree{$fx}{sHead} = $ej-1;
            $$trgTree{$fx}{position} = $fx 
                unless (exists $$trgTree{$fx}{position});
        }
    }
    return $proj;
}

sub many_to_many{

    # many-to-many
    # - two-step procedure: (1) one-to-many (2) many-to-one

    my ($srcWords,$trgWords,$wordSrc2Trg,$wordTrg2Src,$srcTree,$trgTree) = @_;
    my $count = one_to_many($srcWords,$trgWords,$wordSrc2Trg,$wordTrg2Src,$srcTree,$trgTree);
    $count += many_to_one($srcWords,$trgWords,$wordSrc2Trg,$wordTrg2Src,$srcTree,$trgTree);
    return $count;
}


sub one_to_many{
    my ($srcWords,$trgWords,$wordSrc2Trg,$wordTrg2Src,$srcTree,$trgTree) = @_;

    # one-to-many
    # - for each source word that is aligned to multiple target words
    #   --> create a new dummy target word acting as MWU-node
    #   --> link source word only to this new MWU-node (treat as 1:1 link)

    my $transf=0;
    foreach my $ei (0..$#{$srcWords}){
        if ((exists $$wordSrc2Trg{$ei}) && (keys %{$$wordSrc2Trg{$ei}} > 1)){
            push(@{$trgWords},'DUMMY');
            my $fz = $#{$trgWords};
            $transf++;
            my $last=-1;
            foreach my $f (keys %{$$wordSrc2Trg{$ei}}){
                $$trgTree{$f}{head} = $fz;
                $$trgTree{$f}{sDep} = undef;
                $$trgTree{$f}{sHead} = $ei;
                $$trgTree{$f}{position} = $f;
                $last = $f if ($f > $last);
            }

            # link DUMMY word to the selected source word
            # and place it next to the last aligned target word
            # (unless option -e is used)
            $$trgTree{$fz}{sDep} = $ei;
            $$trgTree{$fz}{position} = $opt_e ? $fz : $last+0.1;

            # remove old links and set only the link to the highest node
            foreach (keys %{$$wordSrc2Trg{$ei}}){
                delete $$wordTrg2Src{$_}{$ei};
            }
            foreach (keys %{$wordTrg2Src}){
                unless (keys %{$$wordTrg2Src{$_}}){
                    delete $$wordTrg2Src{$_};
                }
            }

            %{$$wordSrc2Trg{$ei}} = ( $fz => 1 );
            %{$$wordTrg2Src{$fz}} = ( $ei => 1 );
        }
    }
    return $transf;
}


# one-to-many without dummy nodes!
# --> use intersection to decide which node is the head of the MWE
# --> if no link in intersection: use the right-most word
# --> attach all other aligned words to the selected head token (label = mwe)

sub one_to_many_mwe{
    my ($srcWords,$trgWords,
        $interSrc2Trg,$interTrg2Src,
        $wordSrc2Trg,$wordTrg2Src,
        $srcTree,$trgTree) = @_;

    # one-to-many
    # - for each source word that is aligned to multiple target words
    #   --> create a new dummy target word acting as MWU-node
    #   --> link source word only to this new MWU-node (treat as 1:1 link)

    my $transf=0;
    foreach my $ei (0..$#{$srcWords}){
        if ((exists $$wordSrc2Trg{$ei}) && (keys %{$$wordSrc2Trg{$ei}} > 1)){

            $transf++;
            my $fx=undef;
            if (exists $$interSrc2Trg{$ei}){
                ($fx) = keys %{$$interSrc2Trg{$ei}};
            }
            else{
                # otherwise: right-most token!
                # TODO: we could also backoff to standard one_to_many)
                ($fx) = sort {$b <=> $a} keys %{$$wordSrc2Trg{$ei}};
            }

            # safe all links
            my @f = keys %{$$wordSrc2Trg{$ei}};

            # retain only one link
            %{$$wordSrc2Trg{$ei}} = ( $fx => 1 );

            # check the other aligned tokens
            foreach my $fz (@f){
                next if ($fz == $fx);

                # delete the link to the current source token
                delete $$wordTrg2Src{$fz}{$ei};
                unless (keys %{$$wordTrg2Src{$fz}}){
                    delete $$wordTrg2Src{$fz};
                }
                # ignore if the target token is linked to other source tokens
                next if (exists $$wordTrg2Src{$fz});

                unless ($opt_M){
                    # attach node to selected target
                    $$trgTree{$fz}{head} = $fx;
                    $$trgTree{$fz}{sDep} = undef;
                    $$trgTree{$fz}{sHead} = undef;
                    $$trgTree{$fz}{position} = $fz;
                }
            }
        }
    }
    return $transf;
}


sub many_to_one{
    my ($srcWords,$trgWords,$wordSrc2Trg,$wordTrg2Src,$srcTree,$trgTree) = @_;

    # many-to-one
    # - for each target word that is aligned to multiple source words
    #   --> delete all links to source words except for the link
    #       to the one highest up in the source tree (heading the phrase?!)

    my @treeDepth = ();
    foreach my $w (0..$#{$srcWords}){
        $treeDepth[$w] = &get_distance_to_root($w+1,$srcTree);
    }

    my $transf=0;
    foreach my $fx (0..$#{$trgWords}){
        if ((exists $$wordTrg2Src{$fx}) && (keys %{$$wordTrg2Src{$fx}} > 1)){

            $transf++;
            my ($e) = sort { $treeDepth[$a] <=> $treeDepth[$b] } 
                      keys %{$$wordTrg2Src{$fx}};


            # remove old links and set only the link to the highest node
            my %deleted = ();
            foreach (keys %{$$wordTrg2Src{$fx}}){
                delete $$wordSrc2Trg{$_}{$fx};
                $deleted{$_}++;
            }

            # check if there are any links left for the other tokens
            # if not: fix also incoming dependency relations
            # (--> this is not explained in Hwa et al)
            foreach my $ei (keys %{$wordSrc2Trg}){
                unless (keys %{$$wordSrc2Trg{$ei}}){
                    delete $$wordSrc2Trg{$ei};
                    foreach my $ej (keys %{$srcTree}){
                        if ($$srcTree{$ej} == $ei+1){
                            $$srcTree{$ej} = $e+1;
                        }
                    }
                }
            }
            %{$$wordSrc2Trg{$e}} = ( $fx => 1 );
            %{$$wordTrg2Src{$fx}} = ( $e => 1 );
        }
    }
    return $transf;
}


sub unaligned{
    my ($srcWords,$trgWords,$wordSrc2Trg,$wordTrg2Src,$srcTree,$trgTree) = @_;

    # unaligned
    # - for each unaligned source word
    #   --> create empty target word
    #   --> attach incoming and outcoming arcs

    my $total=0;
    my $transf=0;

    do {
        $transf=0;
        foreach my $ej (0..$#{$srcWords}){
            if (! exists $$wordSrc2Trg{$ej}){

                if (! exists $$srcTree{$ej+1}){
                    print STDERR "word $ej has no head!\n";
                    next;
                }

                # project head-relation to new dummy token
                
                my $fy = $#{$trgWords}+1;     # position of new dummy word
                my $ei = $$srcTree{$ej+1}-1;  # head of unaligned source word

                # $ei == -1 means ROOT!
                if ($ei == -1){
                    $$trgTree{$fy}{head} = -1;
                    $$trgTree{$fy}{sDep} = $ej;
                    $$trgTree{$fy}{sHead} = -1;
                }
                elsif (exists $$wordSrc2Trg{$ei}){
                    if (keys %{$$wordSrc2Trg{$ei}}==1){
                        my ($fx) = keys %{$$wordSrc2Trg{$ei}};
                        $$trgTree{$fy}{head} = $fx;
                        $$trgTree{$fy}{sDep} = $ej;
                        $$trgTree{$fy}{sHead} = $ei;
                    }
                    else{
                        # print STDERR "head of $ej is not uniquely aligned!\n";
                        next;
                    }
                }
                else{
                    # print STDERR "head of $ej is not aligned!\n";
                    next;
                }

                push(@{$trgWords},'DUMMY');
                $$wordSrc2Trg{$ej}{$fy} = 1;
                $$wordTrg2Src{$fy}{$ej} = 1;

                # place dummy token next to it's head (unless flag -e is set!)
                # - right of head token if the token aligned to the source head
                #   is to the right of the unaligned source token
                # - left of head token otherwise
                my $h = $$trgTree{$fy}{head};
                my $p = exists $$trgTree{$h} ? $$trgTree{$h}{position} : $h;
                $$trgTree{$fy}{position} = $opt_e ? $fy : $ej > $$trgTree{$fy}{sHead} ? $p+0.2 : $p-0.2;

                $transf++;

                # project incoming relations to new dummy token
#                my $incomingLast = undef;
#                my $incomingFirst = 9999;
                foreach my $ei (0..$#{$srcWords}){
                    if ((exists $$wordSrc2Trg{$ei}) && 
                        (keys %{$$wordSrc2Trg{$ei}}==1)){
                        my ($fx) = keys %{$$wordSrc2Trg{$ei}};
                        if ((exists $$srcTree{$ei+1}) && ($$srcTree{$ei+1} == $ej+1)){
                            $$trgTree{$fx}{head} = $fy;
                            $$trgTree{$fx}{sDep} = $ei;
                            $$trgTree{$fx}{sHead} = $ej;
                            $$trgTree{$fx}{position} = $fx unless (defined $$trgTree{$fx}{position});
#                            $incomingLast = $$trgTree{$fx}{position} 
#                            if ($$trgTree{$fx}{position}>$incomingLast);
#                            $incomingFirst = $$trgTree{$fx}{position} 
#                            if ($$trgTree{$fx}{position}<$incomingFirst);
                        }
                    }
                }
#                # move the position to depending tokens if there are any
#                if ($incomingLast){
#                    $$trgTree{$fy}{position} = $$trgTree{$fy}{position} > $incomingLast ?
#                        $incomingLast+0.1 : $incomingFirst-0.1;
#                }
            }
        }
        $total+=$transf;
    }
    until ($transf==0);
    
    return $total;
}



######################################################################
# delete DUMMY words that have only one incoming and one outgoing arc
# ---> let incoming arc directly point to outgoing goal node
# NEW: ... with the label of the OUTGOING ARC!)

sub delete_unary_dummies{
    my ($trgTree,$words,$sent) = @_;

    my $nrDeleted=0;
    my %incomingArcs=();
    foreach my $f (keys %{$trgTree}){
        $incomingArcs{$$trgTree{$f}{head}}{$f}=1
    }
    foreach my $f (keys %{$trgTree}){
        my $word1 = ($f == int($f)) ? $$words[$f] : 'DUMMY';
        if ($word1 eq 'DUMMY'){
            if (keys %{$incomingArcs{$f}} == 1){
                my ($dep) = keys %{$incomingArcs{$f}};
                my $word2 = ($dep == int($dep)) ? $$words[$dep] : 'DUMMY';
                if ($word2 ne 'DUMMY'){
                    if ($word2 eq 'anderen'){
                        print '';
                    }
                    if (defined $$trgTree{$f}{sDep}){
                        $$trgTree{$dep}{head} = $$trgTree{$f}{head};
                        # $$trgTree{$dep}{sDep} = $$trgTree{$f}{sDep};
                        $$trgTree{$dep}{sHead} = $$trgTree{$f}{sHead};
                        delete $$trgTree{$f};
                        $nrDeleted++;
                    }
                }
            }
        }
    }
    return $nrDeleted;
}
############################################



######################################################################
# delete DUMMY words that have only one incoming and one outgoing arc
# ---> let incoming arc directly point to outgoing goal node

sub delete_unary_dummies_old{
    my ($trgTree,$words,$sent) = @_;

    my $nrDeleted=0;
    my %incomingArcs=();
    foreach my $f (keys %{$trgTree}){
        $incomingArcs{$$trgTree{$f}{head}}{$f}=1
    }
    foreach my $f (keys %{$trgTree}){
        my $word1 = ($f == int($f)) ? $$words[$f] : 'DUMMY';
        if ($word1 eq 'DUMMY'){
            if (keys %{$incomingArcs{$f}} == 1){
                my ($dep) = keys %{$incomingArcs{$f}};
                my $word2 = ($dep == int($dep)) ? $$words[$dep] : 'DUMMY';
                if ($word2 ne 'DUMMY'){
                    if (defined $$trgTree{$f}{sDep}){
                        $$trgTree{$dep}{head} = $$trgTree{$f}{head};
                        $$trgTree{$dep}{sDep} = $$trgTree{$f}{sDep};
                        delete $$trgTree{$f};
                        $nrDeleted++;
                    }
                }
            }
        }
    }
    return $nrDeleted;
}
############################################



######################################################################
# delete DUMMY words that have only one incoming and one outgoing arc
# ---> let incoming arc directly point to outgoing goal node

sub delete_dummy_leaves{
    my ($trgTree,$words,$sent) = @_;

    my $nrDeleted=0;
    my %incomingArcs=();
    foreach my $f (keys %{$trgTree}){
        $incomingArcs{$$trgTree{$f}{head}}{$f}=1
    }
    foreach my $f (keys %{$trgTree}){
        my $word1 = ($f == int($f)) ? $$words[$f] : 'DUMMY';
        if ($word1 eq 'DUMMY'){
            unless (exists $incomingArcs{$f}){
                delete $$trgTree{$f};
                $nrDeleted++;
            }
        }
    }
    return $nrDeleted;
}
############################################



sub check_tree_properties{
    my $trgTree = shift;
    my $verbose = shift;

    my $tree={};
    foreach my $f (keys %{$trgTree}){
        if (defined $$trgTree{$f}{head}){
            $$tree{$f+1}=$$trgTree{$f}{head}+1;
        }
    }

    foreach my $w (keys %{$tree}){
        my $h = $$tree{$w};
        my %visited = ();
        while ($h != 0){
            $visited{$h}++;
            if (! exists $$tree{$h}){
                ## this should not be possible ....
                if ($verbose){
                    print STDERR "Cannot reach ROOT from $h!\n";
                }
                return 0;
            }
            $h = $$tree{$h};
            if (exists $visited{$h}){
                if ($verbose){
                    print STDERR "The tree contains cycles (revisted: $h)!\n";
                }
                return 0;
            }
        }
    }
    return 1;
}
