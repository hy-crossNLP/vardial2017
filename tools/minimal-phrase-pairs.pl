#!/usr/bin/perl

my $srcfile = shift(@ARGV);
my $trgfile = shift(@ARGV);
my $alignfile = shift(@ARGV);

open S,"<$srcfile" || die "cannot read from $srcfile";
open T,"<$trgfile" || die "cannot read from $trgfile";
open A,"<$alignfile" || die "cannot read from $alignfile";


while (<S>){
    chomp;
    my @srcwords = split(/\s+/);

    my $line = <T>;
    chomp($line);
    my @trgwords = split(/\s+/,$line);

    my $line = <A>;
    chomp($line);
    my @alg = split(/\s+/,$line);

    my %src2trg = ();
    my %trg2src = ();

    foreach (@alg){
	my ($s,$t) = split(/\-/);
	$src2trg{$s}{$t} = 1;
	$trg2src{$t}{$s} = 1;
    }

    my %donesrc = ();
    my %donetrg = ();

    foreach my $w (sort {$a <=> $b} keys %src2trg){
	next if ($donesrc{$w});
	my %src=($w=>1);
	my %trg=%{$src2trg{$w}};
	my $added = 1;
	while ($added){
	    $added = 0;
	    foreach my $t (keys %trg){
		foreach (keys %{$trg2src{$t}}){
		    unless (exists $src{$_}){
			$src{$_} = 1;
			$added++;
		    }
		}
	    }
	    foreach my $s (keys %src){
		foreach (keys %{$src2trg{$s}}){
		    unless (exists $trg{$_}){
			$trg{$_} = 1;
			$added++;
		    }
		}
	    }
	}
	my @srcphr = ();
	my @trgphr = ();
	foreach (sort {$a <=> $b} keys %src){ 
	    push(@srcphr,$srcwords[$_]);
	    $donesrc{$_} = 1;
	}
	foreach (sort {$a <=> $b} keys %trg){ 
	    push(@trgphr,$trgwords[$_]);
	    $donetrg{$_} = 1;
	}
	if (@srcphr and @trgphr){
	    print join(' ',@srcphr),"\t",join(' ',@trgphr),"\n";
	}
    }
}
