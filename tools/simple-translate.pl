#!/usr/bin/perl
#
# simple word-by-word translation
# (based on extracted minimal phrases and their frequencies)

use utf8;
use open ':utf8';
use Getopt::Std ;

binmode(STDIN, ':utf8');
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

our ($opt_u,$opt_U);
getopts("uU");


my $phrases = shift(@ARGV);
open F,"<$phrases" || die "cannot open lexicon $phrases!\n";
binmode(STDIN, ':utf8');

my %lex = ();
my $lastS = undef;
my $bestP = 0;

while (<F>){
    chomp;
    # my ($s,$t,$p) = split(/\s+/);
    my ($p,$s,$t);
    if (/^\s*([0-9]+\s+)(\S.*)$/){
	$p = $1;
	($s,$t) = split(/\t/,$2);
	next if ($s=~/\s/);
	next if ($t=~/\s/);
    }
    else{
	next;
    }
    if ($lastS ne $s){
	$lastS=$s;
	$bestP=$p;
	$lex{$s} = $t;
    }
    elsif ($p > $bestP){
	$lex{$s} = $t;
	$bestP=$p;
    }
}
close F;


while (<>){
    chomp;
    next if (/^\#/);
    if (/\S/){
	my @a = split(/\t/);
	if (exists $lex{lc($a[1])}){
	    $a[1] = $lex{lc($a[1])};
	}
	$a[2] = '_';
	$a[4] = '_';
	$a[5] = '_' if ($opt_u);
	$a[7]=~s/\:.*$// if ($opt_U);
	$a[9] = '_';
	print join("\t",@a),"\n";

    }
    else{
	print "\n";
    }
}
