#!/usr/bin/env perl
#
# convert to conllx and eval
#
# USAGE: eval.pl goldfile systemfile
#
#     goldfile ... gold standard file in conllu format
#     systemfile . system output file in conllu format
#

use FindBin qw($Bin);
use File::Temp qw/ :POSIX /;

my $goldfile = shift(@ARGV);
my $systemfile = shift(@ARGV);

my $tmpgold = tmpnam();
my $tmpsys = tmpnam();

system("$Bin/conllu_to_conllx.pl < $goldfile > $tmpgold");
system("$Bin/conllu_to_conllx.pl < $systemfile > $tmpsys");
system("$Bin/eval07.pl -g $tmpgold -s $tmpsys");

