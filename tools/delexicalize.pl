#!/usr/bin/env perl
#
# delexicalize conllu files
#
# USAGE: delexicalize.pl [-uU] < input > output
#
# -u ...... universal PoS only (remove morphological features)
# -U ...... universal dependency relations only
# -s x .... use suffix of x characters (move to feat-field if -p is used!)
# -S x .... second-level suffix substring (only in combination with -s)
#           (this is especially useful with negative values for -s
#            which extract all characters after position -x instead of 
#            the last x characters; with -S y it is possible to get the last 
#            y characters from that substring (excluding characters before
#            position x in the original string))
# -p x .... use prefix of x characters
# -P x .... second-level prefix substring (only in combination with -p)
#           (similar effect as -s x -S y but for prefixes)
# -w ...... leave the word in the data (do not delexicalize)

use utf8;
use open ':utf8';
use Getopt::Std ;

binmode(STDIN, ':utf8');
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

our ($opt_u,$opt_U,$opt_s,$opt_S,$opt_p,$opt_P,$opt_w);
getopts("p:P:s:S:uUw");

while (<>){
    chomp;
    next if (/^\#/);
    if (/\S/){
	my @a = split(/\t/);

	$a[2] = '_';
	$a[4] = '_';
	$a[5] = '_' if ($opt_u);
	$a[7]=~s/\:.*$// if ($opt_U);
	$a[9] = '_';

	if ($opt_s || $opt_p){
	    my $suffix = '';
	    if ($opt_s){
		$suffix = substr($a[1],0-$opt_s);
		$suffix = substr($suffix,0-$opt_S) if ($opt_S);
		$a[1] = $suffix unless ($opt_p);
	    }
	    if ($opt_p){
		$a[1] = substr($a[1],0,$opt_p);
		$a[1] = substr($a[1],0,$opt_P) if ($opt_P);
		if ($opt_s){
		    $a[5] = $suffix;
		}
	    }
	}
	elsif (! $opt_w){
	    $a[1] = '';
	    # $a[1] = '_';
	}
	# $a[1] = '_' unless($a[1]); # in case it's empty
	$_ = join("\t",@a);
    }
    print $_,"\n";
}
