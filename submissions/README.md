
# Results of the final test set submissions

#### Croatian

| Team        | LAS   | UAS   |   |
|-------------|-------|-------|---|
| CUNI        | 60.70 | 69.73 | |
| Helsinki-CLP   | 57.98 | 69.57 | |
| tubasfs    | 55.20 | 66.75 | |
| baseline    | 53.35 | 63.94 | |
| delex (uPoS)| 50.81 | 62.64 | |
| Croatian    | 68.51 | 75.61 | trained on target data |


#### Norwegian

| Team        | LAS   | UAS   | Notes            |
|-------------|-------|-------|------------------|
| CUNI        | 70.21 | 77.13 |                  |
| Helsinki-CLP   | 68.60 | 76.77 |                  |
| tubasfs    | 65.62 | 74.61 | from Swedish     |
| tubasfs    | 64.91 | 73.50 | Danish + Swedish |
| tubasfs    | 58.55 | 67.48 | from Danish      |
| baseline    | 59.95 | 69.02 | Danish + Swedish |
| baseline    | 56.63 | 66.24 | Swedish          |
| baseline    | 54.91 | 64.53 | Danish           |
| delex (uPoS)| 58.80 | 68.58 | Danish + Swedish |
| delex (uPoS)| 57.54 | 66.96 | Swedish          |
| delex (uPoS)| 55.17 | 65.23 | Danish + Swedish |
| Norwegian   | 78.23 | 82.28 | trained on target data|

#### Slovak

| Team        | LAS   | UAS   |   |
|-------------|-------|-------|---|
| CUNI        | 78.12 | 84.92 |   |
| Helsinki-CLP   | 73.14 | 82.87 |   |
| tubasfs    | 64.05 | 73.16 |   |
| baseline    | 53.72 | 65.70 |   |
| delex       | 48.91 | 60.68 |   |
| Slovak      | 69.14 | 76.57 | trained on target data|




