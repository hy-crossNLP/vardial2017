#-*-makefile-*-


# source and target language

ifndef SRC
  SRC = sl
endif
ifndef TRG
  TRG = hr
endif
ifndef WORKDIR
  WORKDIR = ${PWD}/work
endif


TOOLSDIR     = ../tools
PARSERDIR    = ../models
TREEBANKDIR  = ../treebanks
RESULTSDIR   = ../results
UD_TREEBANKS = ../ud-treebanks-v1.4

MATE_PARSER = ${TOOLSDIR}/anna-3.61.jar
HEAPSIZE    = 32g

base-mate: ${PARSERDIR}/${SRC}.parser.mate ${PARSERDIR}/${SRC}-upos.parser.mate
	cp $< ${RESULTSDIR}/${TRG}.${SRC}.mate
	make -f Makefile.mate ${RESULTSDIR}/${TRG}.${SRC}.dev.eval09
	make -f Makefile.mate ${RESULTSDIR}/${TRG}.${SRC}.test.eval09
	rm -f ${RESULTSDIR}/${TRG}.${SRC}.mate
	cp ${word 2,$^} ${RESULTSDIR}/${TRG}.${SRC}-upos.mate
	make -f Makefile.mate ${RESULTSDIR}/${TRG}.${SRC}-upos.dev.eval09
	make -f Makefile.mate ${RESULTSDIR}/${TRG}.${SRC}-upos.test.eval09
	rm -f ${RESULTSDIR}/${TRG}.${SRC}-upos.mate

${PARSERDIR}/${SRC}.parser.conllu: ${TREEBANKDIR}/${SRC}-ud-train.conllu
	cp $< $@

${PARSERDIR}/${SRC}-upos.parser.conllu: ${TREEBANKDIR}/${SRC}-ud-train.conllu
	${TOOLSDIR}/delexicalize.pl -u < $< > $@


supervised: ${WORKDIR}/${TRG}-ud.dev.eval09 ${WORKDIR}/${TRG}-ud.test.eval09

${WORKDIR}/${TRG}-ud.conllu:
	cp ${UD_TREEBANKS}/*/${TRG}-ud-train.conllu $@


##============================================
## generic targets
##============================================

# don't delete intermediate conll09 files in the treebanks dir
.SECONDARY: ${TREEBANKDIR}/${TRG}-ud-predPoS-dev.conll09

## parse development data and evaluate

%.dev.eval09: %.mate ${TREEBANKDIR}/${TRG}-ud-predPoS-dev.conllu \
		${TREEBANKDIR}/${TRG}-ud-predPoS-dev.conll09
	cut -f15 $(word 3,$^) > $@.blank
	cut -f1-8 $(word 3,$^) > $@.before
	cut -f13-15 $(word 3,$^) > $@.after
	paste $@.before $@.blank $@.blank $@.blank $@.blank $@.after |\
	sed "s/^[ \t]*$$//" > $@.test
	java -Xmx${HEAPSIZE} -cp ${MATE_PARSER} is2.parser.Parser \
		-model $< -test $@.test -out $(@:.eval09=.conll09)
	cut -f 1,2,3,5,6,7,10,12,14,15 < $(@:.eval09=.conll09) > $(@:.eval09=.conllu)
	rm -f $(@:.eval09=.conll09) $@.before $@.blank $@.after $@.test
	${TOOLSDIR}/eval.pl $(word 2,$^) $(@:.eval09=.conllu) > $@

%.test.eval09: %.mate ${TREEBANKDIR}/${TRG}-ud-predPoS-test.conllu \
		${TREEBANKDIR}/${TRG}-ud-predPoS-test.conll09
	cut -f15 $(word 3,$^) > $@.blank
	cut -f1-8 $(word 3,$^) > $@.before
	cut -f13-15 $(word 3,$^) > $@.after
	paste $@.before $@.blank $@.blank $@.blank $@.blank $@.after |\
	sed "s/^[ \t]*$$//" > $@.test
	java -Xmx${HEAPSIZE} -cp ${MATE_PARSER} is2.parser.Parser \
		-model $< -test $@.test -out $(@:.eval09=.conll09)
	cut -f 1,2,3,5,6,7,10,12,14,15 < $(@:.eval09=.conll09) > $(@:.eval09=.conllu)
	rm -f $(@:.eval09=.conll09) $@.before $@.blank $@.after $@.test
	${TOOLSDIR}/eval.pl $(word 2,$^) $(@:.eval09=.conllu) > $@



## parse source language data

%.${SRC}.mate.conllu: %.${SRC} \
		${PARSERDIR}/${SRC}.tagger.udpipe \
		${PARSERDIR}/${SRC}.parser.mate
	udpipe -input=horizontal -tag $(word 2,$^) $< > $@.in.conllu
	make -f Makefile.mate $@.in.conll09
	java -Xmx${HEAPSIZE} -cp ${MATE_PARSER} is2.parser.Parser \
		-model $(word 3,$^) -test $@.in.conll09 -out $(@:.conllu=.conll09)
	cut -f 1,2,3,5,6,7,10,12,13,14 < $(@:.conllu=.conll09) > $@
	rm -f $(@:.conllu=.conll09) $@.in.conll09 $@.in.conllu


%.${SRC}.matemarmot.conllu: %.${SRC}.marmot.tagged ${PARSERDIR}/${SRC}.parser.mate
	cp $< $@.in.conllu
	make -f Makefile.mate $@.in.conll09
	java -Xmx${HEAPSIZE} -cp ${MATE_PARSER} is2.parser.Parser \
		-model $(word 2,$^) -test $@.in.conll09 -out $(@:.conllu=.conll09)
	cut -f 1,2,3,5,6,7,10,12,13,14 < $(@:.conllu=.conll09) > $@
	rm -f $(@:.conllu=.conll09) $@.in.conll09 $@.in.conllu



## train a parser model

## pipe symbol makes pre-requisite "order-onlu"
## http://stackoverflow.com/questions/21745816/makefile-make-dependency-only-if-file-doesnt-exist
## ($| lists all order-only pre-requisites)

%.mate: | %.conll09
	java -Xmx${HEAPSIZE} -cp ${MATE_PARSER} is2.parser.Parser \
		-train $| -model $@



# train a model on data that has been tagged with the target language tagger
# (instead of the existing annotation, projected or gold, ...)

%.tagged.mate: ${PARSERDIR}/${TRG}.tagger.udpipe | %.conllu
	mkdir -p $(dir $@)
	${TOOLSDIR}/fix-empty-columns.pl < $| |\
	cut -f2 | tr "\n" ' ' | sed "s/  /\n/g" > $@.in.txt
	udpipe -input=horizontal -tag $< $@.in.txt > $@.in.tagged
	cut -f1-6 $@.in.tagged > $@.first
	cut -f7-10 $| > $@.second
	paste $@.first $@.second | sed "s/^[	]*$$//g" > $@.in.conllu
	make -f Makefile.mate $@.in.conll09
	java -Xmx${HEAPSIZE} -cp ${MATE_PARSER} is2.parser.Parser \
		-train $@.in.conll09 -model $@
	rm -f $@.in.txt $@.in.conllu $@.in.conll09 $@.in.tagged $@.first $@.second

## TODO: this should be prerequisite to the target above
%.tagged.conllu: ${PARSERDIR}/${TRG}.tagger.udpipe | %.conllu
	mkdir -p $(dir $@)
	${TOOLSDIR}/fix-empty-columns.pl < $| |\
	cut -f2 | tr "\n" ' ' | sed "s/  /\n/g" > $@.in.txt
	udpipe -input=horizontal -tag $< $@.in.txt > $@.in.tagged
	cut -f1-6 $@.in.tagged > $@.first
	cut -f7-10 $| > $@.second
	paste $@.first $@.second | sed "s/^[	]*$$//g" > $@
	rm -f $@.in.txt $@.in.tagged $@.first $@.second


%.tagmorph.mate: ${PARSERDIR}/${TRG}.tagger.udpipe | %.conllu
	mkdir -p $(dir $@)
	${TOOLSDIR}/fix-empty-columns.pl < $| |\
	cut -f2 | tr "\n" ' ' | sed "s/  /\n/g" > $@.in.txt
	udpipe -input=horizontal -tag $< $@.in.txt > $@.in.tagged
	cut -f1-3 $@.in.tagged > $@.123
	cut -f4 $| > $@.4
	cut -f5,6 $@.in.tagged > $@.56
	cut -f7-10 $| > $@.7-10
	paste $@.123 $@.4 $@.56 $@.7-10 | sed "s/^[	]*$$//g" > $@.in.conllu
	make -f Makefile.mate $@.in.conll09
	java -Xmx${HEAPSIZE} -cp ${MATE_PARSER} is2.parser.Parser \
		-train $@.in.conll09 -model $@
	rm -f $@.in.txt $@.in.conllu $@.in.conll09 $@.123 $@.4 $@.56 $@.7-10

## TODO: this should be prerequisite to the target above
%.tagmorph.conllu: ${PARSERDIR}/${TRG}.tagger.udpipe | %.conllu
	mkdir -p $(dir $@)
	${TOOLSDIR}/fix-empty-columns.pl < $| |\
	cut -f2 | tr "\n" ' ' | sed "s/  /\n/g" > $@.in.txt
	udpipe -input=horizontal -tag $< $@.in.txt > $@.in.tagged
	cut -f1-3 $@.in.tagged > $@.123
	cut -f4 $| > $@.4
	cut -f5,6 $@.in.tagged > $@.56
	cut -f7-10 $| > $@.7-10
	paste $@.123 $@.4 $@.56 $@.7-10 | sed "s/^[	]*$$//g" > $@
	rm -f $@.in.txt $@.123 $@.4 $@.56 $@.7-10


## convert to conll09

%.conll09: | %.conllu
	grep -v '^#' ${@:.conll09=.conllu} |\
	grep -v '^[0-9]*-[0-9]' |\
	grep -v '^[0-9]\.[0-9]' |\
	perl -e 'while(<>){chomp;if (/\S/){@a=split(/\t/);$$feat=$$a[4];if ($$a[5] ne "_"){$$feat=$$a[5];};print join("\t",$$a[0],$$a[1],"_","_",$$a[3],$$a[3],$$feat,$$feat,$$a[6],$$a[6],$$a[7],$$a[7],"_","_","_");};print "\n";}' > $@


# %.conll09: %.conllu
# 	grep -v '^#' $< |\
# 	grep -v '^[0-9]*-[0-9]' |\
# 	grep -v '^[0-9]\.[0-9]' |\
# 	perl -e 'while(<>){chomp;if (/\S/){@a=split(/\t/);$$feat=$$a[4];if ($$a[5] ne "_"){$$feat=$$a[5];};print join("\t",$$a[0],$$a[1],"_","_",$$a[3],$$a[3],$$feat,$$feat,$$a[6],$$a[6],$$a[7],$$a[7],"_","_","_");};print "\n";}' > $@

#	perl -e 'while(<>){chomp;if (/\S/){@a=split(/\t/);$$a[7]=~s/:.*$$//;$$feat=$$a[4];if ($$a[5] ne "_"){$$feat=$$a[5];};print join("\t",$$a[0],$$a[1],"_","_",$$a[3],$$a[3],$$feat,$$feat,$$a[6],$$a[6],$$a[7],$$a[7],"_","_","_");};print "\n";}' > $@



include Makefile.marmot
