#-*-makefile-*-


# source and target language

ifndef SRC
  SRC = sl
endif

ifndef TRG
  TRG = hr
endif

# corpus to be used for SMT and projection

ifndef CORPUS
  CORPUS = OpenSubtitles2016
endif


# SMT related settings

WORDALIGNTYPE  = grow-diag-final-and
LMORDER        = 5


ifdef CORES
  THREADS = ${CORES}
else
  THREADS = 4
endif


#################################################################

ifndef WORKDIR
  WORKDIR = ${PWD}/work
endif

ifndef CORPUSBASE
  CORPUSBASE = ${WORKDIR}/${CORPUS}.${SRC}-${TRG}
endif


SMTWORKDIR = ${WORKDIR}/${SRC}-${TRG}/${CORPUS}

DATASETS = ${CORPUSBASE}.tok.${SRC} ${CORPUSBASE}.tok.${TRG} \
	   ${CORPUSBASE}.train.${SRC} ${CORPUSBASE}.train.${TRG} \
	   ${CORPUSBASE}.dev.${SRC} ${CORPUSBASE}.dev.${TRG} \
	   ${CORPUSBASE}.test.${SRC} ${CORPUSBASE}.test.${TRG}



OPUSDOWNLOAD = http://opus.lingfil.uu.se/download.php
TOOLSDIR     = ../tools

SMTTOOLS     = ${HOME}/research/tools
MOSESHOME    = ${SMTTOOLS}/mosesdecoder
MOSESSCRIPTS = ${MOSESHOME}/scripts
MOSESBIN     = ${MOSESHOME}/bin
MOSES        = ${MOSESBIN}/moses
MOSESTRAIN   = ${MOSESSCRIPTS}/training/train-model.perl
MOSESMERT    = ${MOSESSCRIPTS}/training/mert-moses.pl

TOKENIZER    = ${MOSESSCRIPTS}/tokenizer

# word alignment tools
FASTALIGN    = time $(SMTTOOLS)/fast_align/build/fast_align -d -o
EFMARAL      = time python3 $(SMTTOOLS)/efmaral/efmaral.py
WORDALIGNER  = ${EFMARAL}
# WORDALIGNER  = ${FASTALIGN}
ATOOLS       = $(SMTTOOLS)/fast_align/build/atools

# language modeling tools
KENLMHOME    = ${SMTTOOLS}/kenlm/build
LMPLZ        = $(KENLMHOME)/bin/lmplz
LMBINARIZE   = $(KENLMHOME)/bin/build_binary
MACHINE      = ${shell uname -m}
SRILMHOME    = ${SMTTOOLS}/srilm/
NGRAMCOUNT   = ${SRILMHOME}/bin/${MACHINE}/ngram-count

# continue with tuning if there is already a run-file
MERTOPT = ${shell if [ -e ${SMTWORKDIR}/tuned/run1.moses.ini ]; then echo '--continue'; fi}



#-------------------------------------------------------------------
.PHONY: phrases-pairs
#-------------------------------------------------------------------

phrase-pairs: ${CORPUSBASE}.phrases.${SRC}-${TRG}

${CORPUSBASE}.phrases.${SRC}-${TRG}: \
		${CORPUSBASE}.train.${SRC} \
		${CORPUSBASE}.train.${TRG} \
		${SMTWORKDIR}/model/aligned.${WORDALIGNTYPE}
	${TOOLSDIR}/minimal-phrase-pairs.pl $^ > $@


#-------------------------------------------------------------------
.PHONY: lm
#-------------------------------------------------------------------

lm: ${CORPUSBASE}.train.${TRG}.${LMORDER}.kenlm

${CORPUSBASE}.train.${TRG}.${LMORDER}.kenlm: ${CORPUSBASE}.train.${TRG}
	${LMPLZ} -o ${LMORDER} < $< > $(@:kenlm=arpa)
	${LMBINARIZE} $(@:kenlm=arpa) $@
	rm -f $(@:kenlm=arpa)


#-------------------------------------------------------------------
.PHONY: lex tm
#-------------------------------------------------------------------

tm: ${SMTWORKDIR}/model/moses.ini

${SMTWORKDIR}/model/moses.ini: \
			${CORPUSBASE}.train.${TRG}.${LMORDER}.kenlm \
			${CORPUSBASE}.train.${SRC} \
			${CORPUSBASE}.train.${TRG} \
			${SMTWORKDIR}/model/aligned.${WORDALIGNTYPE}
	$(MOSESTRAIN) \
		--corpus ${CORPUSBASE}.train \
		--f ${SRC} --e ${TRG} \
		--parallel \
		--lm 0:${LMORDER}:$<:8 \
		--alignment ${WORDALIGNTYPE} \
		--first-step 4 \
		--last-step 9 \
		--root-dir ${SMTWORKDIR}
	rm -f ${SMTWORKDIR}/model/extract*

# 		--max-phrase-length 3 \

${SMTWORKDIR}/model/aligned.${WORDALIGNTYPE}: ${CORPUSBASE}.align.${WORDALIGNTYPE}
	mkdir -p $(dir $@)
	cp $< $@

#-------------------------------------------------------------------
.PHONY: tune
#-------------------------------------------------------------------

tune: ${SMTWORKDIR}/tuned/moses.ini

${SMTWORKDIR}/tuned/moses.ini: \
		${SMTWORKDIR}/model/moses.ini \
		${CORPUSBASE}.dev.${SRC} \
		${CORPUSBASE}.dev.${TRG}
	$(MOSESMERT) \
		--working-dir=$(dir $@) \
		--nbest=200 \
		--mertdir=$(MOSESBIN) \
		--threads=$(THREADS) \
		--decoder-flags='-threads $(THREADS)' \
		${MERTOPT} \
		${word 2,$^} ${word 3,$^} \
		$(MOSES) $<


#-------------------------------------------------------------------
.PHONY: wordalign
#-------------------------------------------------------------------

wordalign: ${CORPUSBASE}.align.${WORDALIGNTYPE}

${CORPUSBASE}.align.%: 	${CORPUSBASE}.train.${SRC}-${TRG}.align \
			${CORPUSBASE}.train.${TRG}-${SRC}.align
	$(ATOOLS) 	-c $(lastword $(subst ., ,$@)) \
			-i ${word 1,$^} -j ${word 2,$^} > $@

${CORPUSBASE}.train.${SRC}-${TRG}: ${CORPUSBASE}.train.${SRC} ${CORPUSBASE}.train.${TRG}
	paste $^ | sed 's/	/ ||| /' > $@

${CORPUSBASE}.train.${SRC}-${TRG}.align: ${CORPUSBASE}.train.${SRC}-${TRG}
	${WORDALIGNER} -i $< > $@

${CORPUSBASE}.train.${TRG}-${SRC}.align: ${CORPUSBASE}.train.${SRC}-${TRG}
	${WORDALIGNER} -i $< -r > $@



#-------------------------------------------------------------------
.PHONY: data
#-------------------------------------------------------------------

.SECONDARY: ${DATASETS}
data: ${DATASETS}


space :=
space +=
SORTEDLANG = $(subst $(space),-,$(sort ${SRC} $(TRG)))

${CORPUSBASE}.zip:
	wget -O $@ ${OPUSDOWNLOAD}?f=${CORPUS}%2F${SORTEDLANG}.txt.zip
	( cd $(dir $@); unzip $(notdir $@) )
ifneq (${SORTEDLANG},${SRC}-${TRG})
	mv ${WORKDIR}/${CORPUS}.${SORTEDLANG}.${SRC} ${CORPUSBASE}.${SRC}
	mv ${WORKDIR}/${CORPUS}.${SORTEDLANG}.${TRG} ${CORPUSBASE}.${TRG}
endif
	rm -f ${WORKDIR}/${CORPUS}.${SORTEDLANG}.ids

${CORPUSBASE}.${SRC} ${CORPUSBASE}.${TRG}: | ${CORPUSBASE}.zip


# data splits

%.test.$(SRC): %.clean.$(SRC)
	head -5000 $< > $@

%.dev.$(SRC): %.clean.$(SRC)
	head -10000 $< | tail -5000 > $@

%.train.$(SRC): %.clean.$(SRC)
	tail -n +10001 $< > $@

%.test.$(TRG): %.clean.$(SRC)
	head -5000 $(<:.${SRC}=.${TRG}) > $@

%.dev.$(TRG): %.clean.$(SRC)
	head -10000 $(<:.${SRC}=.${TRG}) | tail -5000 > $@

%.train.$(TRG): %.clean.$(SRC)
	tail -n +10001 $(<:.${SRC}=.${TRG}) > $@

%.clean.$(SRC): %.tok.$(SRC) %.tok.$(TRG)
	$(MOSESSCRIPTS)/training/clean-corpus-n.perl \
                $(<:.${SRC}=) $(SRC) $(TRG) $(@:.${SRC}=) \
		0 100 $(@:.${SRC}=.lines)

%.tok.$(SRC): %.$(SRC)
	cat $< |\
	$(TOKENIZER)/replace-unicode-punctuation.perl |\
	$(TOKENIZER)/remove-non-printing-char.perl |\
	$(TOKENIZER)/normalize-punctuation.perl -l $(SRC) |\
	$(TOKENIZER)/pre-tokenizer.perl -l $(SRC) |\
	$(TOKENIZER)/tokenizer.perl -no-escape -threads ${THREADS} -l $(SRC) |\
	$(TOKENIZER)/lowercase.perl |\
	$(TOKENIZER)/escape-special-chars.perl |\
	sed -e "s/&apos\;/'/g" -e 's/&quot\;/\"/g' > $@

%.tok.$(TRG): %.$(TRG)
	cat $< |\
	$(TOKENIZER)/replace-unicode-punctuation.perl |\
	$(TOKENIZER)/remove-non-printing-char.perl |\
	$(TOKENIZER)/normalize-punctuation.perl -l $(TRG) |\
	$(TOKENIZER)/pre-tokenizer.perl -l $(TRG) |\
	$(TOKENIZER)/tokenizer.perl -no-escape -threads ${THREADS} -l $(TRG) |\
	$(TOKENIZER)/lowercase.perl |\
	$(TOKENIZER)/escape-special-chars.perl |\
	sed -e "s/&apos\;/'/g" -e 's/&quot\;/\"/g' > $@

#--------------------

clean:
	rm -f ${DATASETS}
	rm -f ${CORPUSBASE}.clean.*
	rm -f ${CORPUSBASE}.align.*
	rm -f ${CORPUSBASE}.train.*
	rm -f ${CORPUSBASE}.phrases.*
	rm -fr ${SMTWORKDIR}
