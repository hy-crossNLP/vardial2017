
# Baseline models for cross-lingual parsing at VarDial 2017

All models have been trained using [UDPipe](http://ufal.mff.cuni.cz/udpipe) with default settings:

* `xx.tagger.udpipe`: Tagger for PoS and morphological annotation for language <xx>
* `xx.parser.udpipe`: fully supervised parsing model for language <xx>
* `xx-delex.parser.udpipe`: delexicalized parsing model trained on language <xx>

